export default {
  items: [
    // {
    //   name: 'Dashboard',
    //   url: '/admin/dashboard',
    //   icon: 'icon-speedometer'
    // },
    {
      title: true,
      name: 'APP MANAGEMENT',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      },
    },

    {
      name: 'Users',
      url: '/admin/users',
      icon: 'fa fa-users',
      auth: {
        permissions: ['view_users']
      }
    },
    {
      name: 'Roles',
      url: '/admin/roles',
      icon: 'fa fa-sitemap',
      auth: {
        permissions: ['view_roles']
      }
    },
    {
      name: 'Categories',
      url: '/admin/categories',
      icon: 'fa  fa-chevron-circle-right',
      auth: {
        permissions: ['view_categories']
      }
    },
    {
      name: 'Codes',
      url: '/admin/promocodes',
      icon: 'fa  fa-chevron-circle-right',
      auth: {
        permissions: ['view_promocodes']
      }
    },
    {
      name: 'Submissions',
      url: '/admin/submissions',
      icon: 'fa fa-chevron-circle-right',
      auth: {
        permissions: ['view_submissions']
      },
      children: [
        {
          name: 'List',
          url: '/admin/submissions/list',
          auth: {
            permissions: ['view_submissions']
          }
        }
      ]
    },
  ]
}
