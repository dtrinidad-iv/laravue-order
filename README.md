# PROJECTORDER2019

## Getting Started


### Install from repository

Download & unpack the files, navigate to the directory and run:

    composer install

After it has completed, run:

    npm install

Copy the example .env file:

    cp .env.example .env

Generate an application key:

    php artisan key:generate

Run Mix tasks:

    npm run dev
    npm run watch //build every time you have changes
    npm run prod

Seed:

    php artisan db:seed
    php artisan db:seed --class=RolePermissionsSeeder
