<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('code');
            $table->text('first_name');
            $table->text('middle_name');
            $table->text('last_name');
            $table->text('street_name');
            $table->text('barangay');
            $table->text('city');
            $table->text('mobile_number');
            $table->text('email_address');
            $table->boolean('is_confirmed')->default(false);
            $table->boolean('is_valid')->default(true);
            $table->boolean('is_available')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submissions');
    }
}
